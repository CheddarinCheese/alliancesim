﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllianceManager : MonoBehaviour {

    public List<Alliance> totalAlliances;
    public GameObject alliancePanel;

    // Defult method
	private void Awake()
	{
        alliancePanel.SetActive(false);
	}
    // Button method
	public void closePanel(){
        alliancePanel.SetActive(false);
    }

    public void openPanel(){
        alliancePanel.SetActive(true);
    }
     
    // Actual stuff :D
    private float calculateRelation(Country country1, Country country2){
        // 0-5
        float strength = 0f;

        //Religion
        if(country1.religion == Country.Religion.NONE){
            if (country2.religion == Country.Religion.NONE)
            {
                strength += 1f;
            } else if (country2.religion == Country.Religion.ALEXANDERITE) {
                strength += .5f;
            }
        } else if (country1.religion == Country.Religion.ALEXANDERITE){
            if (country2.religion == Country.Religion.ALEXANDERITE)
            {
                strength += 1f;
            } else if (country2.religion == Country.Religion.NONE){
                strength += .5f;
            } else {
                strength -= .5f;
            }
        } else{
            if (country2.religion == Country.Religion.ALEXANDERITE)
            {
                strength -= .5f;
            }
            else if (country2.religion == Country.Religion.MATTIST)
            {
                strength += 1.5f;
            }
        }

        //Goverment
        if(country1.government == Country.Government.MONARCHY){
            if(country2.government == Country.Government.MONARCHY){
                strength += 1f;
            } 
        } else if (country1.government == Country.Government.DEMOCRACY){
            if(country2.government == Country.Government.DEMOCRACY){
                strength += 1f;
            } else {
                strength += .5f;
            }
        }

        //Econemy
        if(country1.econemy== country2.econemy){
            strength += 1f;
        }

        return Mathf.Clamp(strength * 2, 0f, 5f);
    }

    private float calculateAllianceDependency(Country country1, Country country2){
        float gdpDif = country1.gDP - country2.gDP;
        float militaryStrengthDif = country1.militaryStrength - country2.militaryStrength;
        float techDif = country1.tech - country2.tech;

        return (gdpDif + militaryStrengthDif + techDif) / 3f;

    }

    public float getAllianceRelationShip(Country country1, Country country2){
        return calculateRelation(country1, country2) + calculateAllianceDependency(country1, country2);
    }

    public float calculateAllianceStrength(Alliance alliance){
        // 0-5
        float strength = 0f;
        for (int i = 0; i < alliance.countries.Count-1; i++){
            for (int k = i+1; i < alliance.countries.Count-2; k++){
                if (k < alliance.countries.Count && i < alliance.countries.Count && k >= 0  && i >= 0)
                {
                    strength += getAllianceRelationShip(alliance.countries[i], alliance.countries[k]);
                } else {
                    strength += .2f;
                }
            }
        }
        return Mathf.Clamp(strength,0,5);
    }

    public List<Alliance> findAlliances(Country country)
    {
        List<Alliance> list = new List<Alliance>();
        foreach (Alliance alliance in totalAlliances){
            foreach (Country coun in alliance.countries){
                if(coun.name.Equals(country.name)){
                    list.Add(alliance);
                }
            }
        }
        return list;
    }

    public void commonWealthAlliance(){
        foreach (Alliance alliance in totalAlliances){
            alliance.commonWealth();
        }
    }
}
