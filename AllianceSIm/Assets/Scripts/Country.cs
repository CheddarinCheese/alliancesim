﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "My Country")]
public class Country : ScriptableObject {
    

    public new string name;
    public Transform capitalTransform;
    ///<summary> Mattist(More extreme) and Alexanderites hate eachother, and strongly support there own kind, aethiests are nuetral but avoid mattists</summary>
    public enum Religion { MATTIST, ALEXANDERITE, NONE };
    public Religion religion;
    /// <summary> Monarcies tend to favor eachother and hates democracies. Democracies favor only democracies </summary>
    public enum Government { MONARCHY, DICTATORSHIP, DEMOCRACY };
    public Government government;
    /// <summary> Capitalist rely on alliances and trade, communists are more self sustaining. </summary>
    public enum Econemy { CAPITALIST, COMMUNIST };
    public Econemy econemy;

    [Header("Each Stat is ranked from 0 - 5")]
    /// <summary> The gdp can be used to enhance military or tech. </summary>
    public float gDP;
    ///<summary> The stength of the country when at war</summary>
    public float militaryStrength;
    /// <summary> The work force, determines how efficient your country is </summary>
    public float PopulationPerCapita;
    ///<summary> Tech determines how fast you can increase military stength</summary>
    public float tech;

    [HideInInspector]
    public bool isDead = false;

    //Apperance
    [Header("Design")]
    public Color color;
    public Sprite flag;

    //Ranking 
    /// <summary>
    /// First World is Good, Second World is medium, and Thrid world is muy mal
    /// </summary>
    public string getCondition(){
        float rank = gDP + militaryStrength + tech;
        if (rank < 6)
            return "Thrid World";
        else if (rank < 11)
            return "Second World";
        else
            return "First World";
    }

    public int getRank(){
        float rank = gDP + militaryStrength + tech;
        return (int)(rank / 3f);
    }


}
