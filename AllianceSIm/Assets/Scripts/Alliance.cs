﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Alliance : MonoBehaviour
{
    /// <summary> The name of the alliance; </summary>
    public new string name;
    /// <summary> Countries that are in the alliance. </summary>
    public List<Country> countries;
    //The strength of the alliance 1 - 5, 1 is about to break, 5 is very stable
    public float strength;
    ///<summary> The basis of there alliance</summary>
    /// Trade increases tech of weaker country (proximity)
    /// Military defends eachother when attacked (
    public enum Purpose { MILITARY, TRADE, NONAGGRESSION };
    public Purpose purpose;

    private void Start()
    {
        GetComponent<Text>().text = name;
    }
    public void openDescription()
    {
        GameMaster.description.SetActive(false);
        GameMaster.allianceDescription.SetActive(true);
        GameMaster.allianceDescription.GetComponent<AllianceDesciption>().setValues(this);
    }

    public void openTurnPanel()
    {
        GameMaster.turnPanelAlliance.SetActive(true);
        GameMaster.turnManager.selectedAlliance = this;
    }

    public void stringToPurpose(string text)
    {
        if (text.Equals("Trade"))
             purpose = Purpose.TRADE;
        else if (text.Equals("Military"))
            purpose = Purpose.MILITARY;
        else if (text.Equals("Non Aggression"))
            purpose = Purpose.NONAGGRESSION;
        else
            Debug.LogError("OHHHH NO MIS SPELLED SOMETHING!");
       
    }

    public void commonWealth(){
        float totalGdp = 0f;
        float totalTech = 0f;
        float totalMilitary = 0f;
        foreach(Country country in countries){
            totalGdp += country.gDP;
            totalTech += country.tech;
            totalMilitary += country.militaryStrength;
        }

        foreach (Country country in countries)
        {
            if(purpose == Purpose.TRADE)
                country.gDP += (totalGdp * strength)/(3*countries.Count) * (country.PopulationPerCapita/5);
            else if (purpose == Purpose.NONAGGRESSION)
                country.tech += (totalTech * strength)/(3*countries.Count) * (country.gDP/5);
            else
                country.militaryStrength += (totalMilitary*strength) /(3f * countries.Count) * (country.tech/5);

            country.tech = Mathf.Clamp(country.gDP, 0f, 5f);
            country.militaryStrength = Mathf.Clamp(country.tech, 0f, 5f);
            country.gDP = Mathf.Clamp(country.militaryStrength, 0f, 5f);
        }


    }


}
