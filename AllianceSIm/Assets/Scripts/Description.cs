﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Description : MonoBehaviour {

    public Text nameText;
    public Text rankText;
    public Text government;
    public Text econemy;
    public Text religion;
    public Text gDP;
    public Text military;
    public Text tech;
    public Text popPerCap;
    public Image flag;

	private void Awake()
	{
        gameObject.SetActive(false);
	}

    public void setValues(Country country){
        nameText.text = country.name;
        rankText.text = country.getCondition();
        government.text = country.government.ToString();
        econemy.text = country.econemy.ToString();
        religion.text = country.religion.ToString();
        gDP.text = "GDP: " + country.gDP.ToString();
        military.text = "Military: " + country.militaryStrength.ToString();
        tech.text = "Tech: " + country.tech.ToString();
        popPerCap.text = "PopPerCap: " + country.PopulationPerCapita.ToString();
        flag.sprite = country.flag;
    }

    public void close(){
        gameObject.SetActive(false);
    }
}
