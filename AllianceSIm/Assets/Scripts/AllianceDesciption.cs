﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AllianceDesciption : MonoBehaviour {

    public Color green;
    public Color red;

    public Text nameText;
    public Image strengthBar;
    public Text type;
    public Text[] memberList;

	private void Awake()
	{
        gameObject.SetActive(false);
	}
	public void setValues(Alliance alliance){
        strengthBar.fillAmount = alliance.strength / 5;
        strengthBar.color = Color.Lerp(red, green, alliance.strength / 5);
        nameText.text = alliance.name;
        type.text = alliance.purpose.ToString();

        int i = 0;
        foreach(Country country in alliance.countries){
            memberList[i].text = country.name;
            i++;
        }
    }

    public void close(){
        gameObject.SetActive(false);
    }
}
