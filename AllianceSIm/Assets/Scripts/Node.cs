﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {
    
    ///<summary> The country that controls it</summary>
    public Country country;
    /// <summary> Description Box </summary>
    private GameObject descriptionBox;

	private void Start()
	{
        descriptionBox = GameMaster.description;
        if (country != null)
        {
            GetComponent<SpriteRenderer>().color = country.color;
        }
	}


	private void OnMouseDown()
	{
        if (country != null)
        {
            if (GameMaster.turnManager.isSelectingCountry)
            {
                GameMaster.turnManager.selectedCountry = country;

                if (GameMaster.turnManager.isCreatingAlliance)
                {
                    GameMaster.turnManager.createAlliance();
                }
                else
                {
                    GameMaster.turnManager.addToAlliance();
                }

                GameMaster.turnManager.isSelectingCountry = false;
            }
            else
            {
                GameMaster.allianceDescription.SetActive(false);
                descriptionBox.SetActive(true);
                descriptionBox.GetComponent<Description>().setValues(country);
            }
        }
	}
	private void OnDrawGizmos(){
        if (country != null)
        {
            Gizmos.color = country.color;
            Gizmos.DrawSphere(transform.position, .5f);
        }
	}
}

