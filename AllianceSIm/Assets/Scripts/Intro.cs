﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Intro : MonoBehaviour {

    public Text nameText;
    public Country myCountry;
    public Text turnText;
	public void exit()
	{
        myCountry.name = nameText.text;
        turnText.text = "1 : "+ myCountry.name;
        gameObject.SetActive(false);
	}
}
