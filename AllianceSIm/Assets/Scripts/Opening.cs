﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opening : MonoBehaviour {

    public GameObject simulationCanvas;

	public void Update()
	{
        if(Input.GetKeyDown(KeyCode.Space)){
            simulationCanvas.SetActive(true);
            Destroy(this.gameObject);
        }

	}
}
