﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Notification : MonoBehaviour {

    [SerializeField]
    private GameObject AlertPrefab;

    public GameObject simCan;
    public void alert(string str, Sprite image){
        GameObject instance = Instantiate(AlertPrefab, simCan.transform) as GameObject;
        instance.GetComponentInChildren<Text>().text = str;
        instance.GetComponentInChildren<Alert>().Image.sprite = image;

    }
}
