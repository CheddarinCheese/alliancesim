﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TurnManager : MonoBehaviour
{
    public List<Country> totalCountries;
    //UI stuff
    public Text createName;
    public Text purpose;
    public Text turnText;
    public GameObject panel;
    [Header("UI Lists")]
    public GameObject allianceList;
    public GameObject allianceTurnList;
    [Header("Prefabs")]
    public GameObject allianceGO;
    public GameObject allianceTurnGO;
    [Header("Icons")]
    public Sprite alignIcon;
    public Sprite unAlignIcon;
    public Sprite strengthenIcon;
    public Country currentCountry;
    public Sprite deathIcon;
    public AudioSource clip;

    [HideInInspector]
    public int turn = 1;

    // Your country's Desicions[
    [HideInInspector]
    public bool isSelectingCountry = false;
    //[HideInInspector]
    public Country selectedCountry;
    [HideInInspector]
    public Alliance selectedAlliance;
    [HideInInspector]
    public bool isCreatingAlliance = false;
    [HideInInspector]
    public Country countryTurn;

    public void createAlliance()
    {
        Debug.Log(GameMaster.allianceManager.getAllianceRelationShip(currentCountry, selectedCountry));
        if (GameMaster.allianceManager.getAllianceRelationShip(currentCountry, selectedCountry) > 2.5f)
        {
            GameMaster.notification.alert(selectedCountry.name + " Alligned With " + currentCountry.name, alignIcon);
            GameObject newAlliance = Instantiate(allianceGO, allianceList.transform) as GameObject;
            Alliance alliance = newAlliance.GetComponent<Alliance>();
            alliance.name = createName.text;
            alliance.countries.Add(currentCountry);
            alliance.countries.Add(selectedCountry);
            alliance.stringToPurpose(purpose.text);
            alliance.strength = GameMaster.allianceManager.getAllianceRelationShip(currentCountry, selectedCountry);
            GameMaster.allianceManager.totalAlliances.Add(alliance);
            GameObject newAllianceTurn = Instantiate(allianceTurnGO, allianceTurnList.transform) as GameObject;
            newAllianceTurn.GetComponent<Text>().text = alliance.name;
            newAllianceTurn.GetComponent<AllianceReference>().alliance = alliance;
            newAllianceTurn.SetActive(true);
            clip.Play();
            purpose.text = "Trade";
            createName.text = "Name";
        }
        else
        {
            GameMaster.notification.alert(selectedCountry.name + "Refused to Allign With " + currentCountry.name, unAlignIcon);

        }
        isCreatingAlliance = false;
        GameMaster.turnPanelCreate.SetActive(false);
        playerEndTurn();
    }
    public void addToAlliance()
    {
        selectedAlliance.countries.Add(selectedCountry);
        GameMaster.notification.alert(selectedCountry.name + " is now apart of " + selectedAlliance.name, alignIcon);
        selectedAlliance.strength = GameMaster.allianceManager.calculateAllianceStrength(selectedAlliance);
        GameMaster.turnPanelAlliance.SetActive(false);
        playerEndTurn();
    }

    //Button Related
    public void selectCountryAlliance()
    {
        isSelectingCountry = true;
    }
    public void selectCountryCreate()
    {
        isSelectingCountry = true;
        isCreatingAlliance = true;
    }

    public void strengthenAlliance()
    {
        GameMaster.notification.alert(selectedCountry.name + " Strengthened " + selectedAlliance.name, strengthenIcon);
        selectedAlliance.strength = Mathf.Clamp(selectedAlliance.strength + .2f, 0f, 5f);
        playerEndTurn();
    }
    public void openPanelAlliance(AllianceReference reference)
    {
        selectedAlliance = reference.alliance;
        GameMaster.turnPanelAlliance.SetActive(true);
        GameMaster.turnPanelCreate.SetActive(false);
    }
    public void openPanelCreate()
    {
        GameMaster.turnPanelCreate.SetActive(true);
        GameMaster.turnPanelAlliance.SetActive(false);
    }

    public void endTurn()
    {
        panel.SetActive(false);
        turn++;
        //Problem code
        Debug.Log((int)((turn - 1f) % 11f));
        currentCountry = totalCountries[(int)((turn - 1f) % 11f)];

        if ((int)((turn - 1f) % 11f) == 0)
        {
            startMyTurn();
            return;
        }
        turnText.text = turn + " : " + currentCountry;
        StartCoroutine(aiTurnAction());
    }


    public void playerEndTurn()
    {
        
        if ((int)((turn - 1f) % 11f) == 0)
            endTurn();
    }

    public void startMyTurn()
    {
        panel.SetActive(true);
    }


    //Turns

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameMaster.turnPanelCreate.SetActive(false);
            GameMaster.turnPanelAlliance.SetActive(false);
        }



    }

    private IEnumerator aiTurnAction()
    {
        float ran = Random.Range(0, 100);
        selectedCountry = totalCountries[(int)Random.Range(1, 9)];
        if (ran > 80f)
        {
            float randy = Random.Range(0, 2);
            if (randy == 0)
            {
                purpose.text = "Military";
                createName.text = "Military Pact of " + turn;
            }
            else if (randy == 1)
            {
                purpose.text = "Trade";
                createName.text = "Trade Pact of " + turn;
            }
            else
            {
                purpose.text = "Non Aggression";
                createName.text = "Backoff bro of " + turn;
            }
            createAlliance();


        }
        else if (ran > 30f)
        {
            List<Alliance> list = GameMaster.allianceManager.findAlliances(currentCountry);
            if (list.Count != 0)
            {
                int rand = (int)Random.Range(0, list.Count-1);
                selectedAlliance = list[rand];
                if (ran > 55f)
                {
                    strengthenAlliance();
                }
                else
                {
                    addToAlliance();
                }
            }
        }
        yield return new WaitForSeconds(3f);
        GameMaster.allianceManager.commonWealthAlliance();
        countryDepreciation();
        endTurn();
    }

    private void countryDepreciation(){
        foreach (Country country in totalCountries)
        {
            if (!country.isDead)
            {
                country.tech -= .02f;
                country.militaryStrength -= .02f;
                country.gDP -= .03f;

                country.tech = Mathf.Clamp(country.gDP, 0f, 5f);
                country.militaryStrength = Mathf.Clamp(country.tech, 0f, 5f);
                country.gDP = Mathf.Clamp(country.militaryStrength, 0f, 5f);

                if (country.getRank() <= 1)
                {
                    country.PopulationPerCapita -= .1f;
                }
                else if (country.getRank() == 2)
                {
                    country.PopulationPerCapita += .05f;
                }
                else
                {
                    country.PopulationPerCapita += .1f;
                }

                country.PopulationPerCapita = Mathf.Clamp(country.PopulationPerCapita, 0f, 5f);

                if (country.PopulationPerCapita <= 0f)
                {
                    Node[] gameList = FindObjectsOfType<Node>();
                    foreach (Node node in gameList)
                    {
                        if (node.country != null)
                        {
                            if (node.country.name.Equals(country.name))
                            {
                                node.GetComponent<SpriteRenderer>().color = Color.black;
                            }
                        }
                    }
                    GameMaster.notification.alert(country.name + " Died! ", deathIcon);
                    country.isDead = true;
                }
            }
        }
    }
}
