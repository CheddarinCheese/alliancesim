﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour {

    [SerializeField]
    private GameObject inspectorDescription;
    public static GameObject description;

    [SerializeField]
    private GameObject inspectorAllianceDescription;
    public static GameObject allianceDescription;

    [SerializeField]
    private GameObject inspectorTurnPanelAlliance;
    public static GameObject turnPanelAlliance;

    [SerializeField]
    private GameObject inspectorTurnPanelCreate;
    public static GameObject turnPanelCreate;

    [SerializeField]
    private GameObject inspectorTurnPanel;
    public static GameObject turnPanel;

    [SerializeField]
    private TurnManager inspectorTurnManager;
    public static TurnManager turnManager;
    public static Country myCountry;

    [SerializeField]
    private AllianceManager inspectorAllianceManager;
    public static AllianceManager allianceManager;

    [SerializeField]
    private Notification inspectorNotification;
    public static Notification notification;

	private void Awake()
	{
        description = inspectorDescription;
        allianceDescription = inspectorAllianceDescription;
        turnManager = inspectorTurnManager;
        turnPanelAlliance = inspectorTurnPanelAlliance;
        turnPanelCreate = inspectorTurnPanelCreate;
        turnPanel = inspectorTurnPanel;
        allianceManager = inspectorAllianceManager;
        notification = inspectorNotification;
        turnPanelAlliance.SetActive(false);
        turnPanelCreate.SetActive(false);
	}
}
